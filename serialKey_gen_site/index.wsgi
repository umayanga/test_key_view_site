import os
import sys
import site

# Add the site-packages of the chosen virtualenv to work with
site.addsitedir('~/Desktop/view_site/serialKey_gen_site/lib/python3.4/site-packages')

# Add the app's directory to the PYTHONPATH
sys.path.append('/Desktop/view_site/serialKey_gen_site')
sys.path.append('/Desktop/view_site/serialKey_gen_site/mysite')

os.environ['DJANGO_SETTINGS_MODULE'] = 'mysite.settings'

# Activate your virtual env
activate_env=os.path.expanduser("Desktop/view_site/serialKey_gen_site/myvenv/bin/activate_this.py")
execfile(activate_env, dict(__file__=activate_env))

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

