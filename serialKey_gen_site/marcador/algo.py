import string
import random

class algo:

    a = 0;
    b = 0;
    alist = [];


    def __init__(self,b_p_name, quantity,alist):
        self.a = b_p_name
        self.b = quantity
        self.alist = alist
        if(len(str(self.a))< 2):
            self.a =str(self.a).zfill(2)


    def id_generator(self,size=5, chars=string.ascii_uppercase + string.digits):
        rebetable = True;
        for x in range(0, self.b):
             rebetable = True
             obj = str(self.a) + ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(size))
             if obj not in self.alist:
                  self.alist.append(obj)
             else:
                 while(rebetable):
                     obj = str(self.a) + ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(size))
                     if obj not in self.alist:
                         self.alist.append(obj)
                         rebetable = False


        return self.alist;
