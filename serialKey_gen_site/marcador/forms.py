from django import forms
from django.forms import ModelForm
from django.contrib.admin import widgets
from .models import Key_Gen,Brand,Pack_Size,Promotion ,Product_Code



#define form
class KeyGenarateForm(ModelForm):

    promotion_detail = forms.CharField(widget=forms.Textarea,required=False ,max_length=250)

    class Meta:
        model = Key_Gen
        fields = ['key_quantity' ,'batch_detail','promotion_detail']
        #fields = ['key_beging_code', 'key_quantity' ,'batch_detail','promotion_detail']
        exclude = ('key_code','expier_date','date_created', 'date_updated')



class BrandGenarateForm(ModelForm):

    class Meta:
        model = Brand
        fields = ['brand_name']
        exclude = ('brand_code','date_created')




class Pack_SizeGenarateForm(ModelForm):

    class Meta:
        model = Pack_Size
        fields = ['pack_size_name']
        exclude = ('pack_size_code','date_created')




class PromotionGenarateForm(ModelForm):

    class Meta:
        model = Promotion
        fields = ['promotion_name']
        exclude = ('promotion_code','date_created')






class Product_CodeGenarateForm(ModelForm):


    class Meta:
        model = Product_Code
        exclude = ('date_created',)




