# encoding: utf-8
from django.contrib.auth.models import User
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.timezone import now
from .algo import  algo
from django.http import HttpResponse
from pymongo import MongoClient
from datetime import datetime
from django.utils import timezone

from django.core.validators import MaxValueValidator, MinValueValidator



@python_2_unicode_compatible
class Key_Gen(models.Model):
    #key_beging_code =models.DecimalField('Brand_name +Pack_size' ,max_digits=5, decimal_places=2)
    ##key_beging_code =models.IntegerField("Product ", choices=PRODUCTS)
    #key_quantity    =models.DecimalField("key Quantity" ,max_digits=19, decimal_places=10 )
    key_quantity  = models.IntegerField("key Quantity",default=1, validators=[MaxValueValidator(100000),MinValueValidator(1)] ,help_text="please ,Enter key quantity between 1 - 100000")
    batch_detail  = models.CharField("Batch Code",max_length=20)
    promotion_detail  = models.TextField("Promotion" ,max_length=250 ,unique=False)
    expier_date = models.DateField("Expiry Date")
    key_code    = models.CharField("key Code" ,max_length=50 , unique=True)
    date_created = models.DateTimeField("date created",auto_now_add=True)
    date_updated = models.DateTimeField("date updated",auto_now_add=True)



    class Meta:
        verbose_name='key_Gen'
        verbose_name_plural='keys_Gen'
        ordering = ['-date_created']


    def __str__(self):
        return '%s' % (self.key_code)


    def adddate(self, *args, **kwargs):
        if not self.id:
            self.date_created = timezone.localtime(timezone.now())
        self.date_updated = timezone.localtime(timezone.now())
        #print(type(timezone.localtime(timezone.now())))


    def get_key_code(self,x,y):
        alist = []
        x = algo(int(x),int(y),alist)
        keys =x.id_generator()
        return keys

    def add_expiry_date(self,x):
        self.expier_date = x;


    def save(self, *args, **kwargs):

        self.adddate()
        self.expier_date =str(args[0])
        self.key_beging_code = int(args[1])

        y = self.expier_date.replace("/","")
        self.expier_date = datetime.strptime(y, "%m%d%Y");

        #create connection
        client = MongoClient()

        #create emty list
        alist = []
        llist = []

        ################Batch Detail######################

        db1 = client.product.batch_code
        batch_record = {}

        #find table existing entry that code
        batch_record = {'batch_code': self.batch_detail ,'product_code':self.key_beging_code ,'key_quntity' : self.key_quantity,'created_date':self.date_created}

        try:
            db1.insert_one(batch_record)
        except:
            return False

        ##########################################################




        #make database_table
        db = client.product.serial_key
        keyrecord_record = {}

        #find table existing entry that code
        f_cursor = db.find({"b_p_code": int(self.key_beging_code)})

        for document in f_cursor:
              alist.append(document['key'])
              llist.append(document['key'])

        x = algo(int(self.key_beging_code),int(self.key_quantity),alist)
        keys =x.id_generator()

        in_first = set(llist)
        in_second = set(alist)
        in_second_but_not_in_first = in_second - in_first

        keys = list(in_second_but_not_in_first)
        keys.sort(reverse=True)

        k=0;
        for x in keys:
            k +=1;
            keyrecord_record = {'key':keys[k-1],'b_p_code':int(self.key_beging_code),'expier_date' :self.expier_date ,'created_date':self.date_created,'batch_detail':self.batch_detail,'promotion_detail':self.promotion_detail }
            db.insert(keyrecord_record)






@python_2_unicode_compatible
class Brand(models.Model):
    brand_code    = models.IntegerField("Brand Code")
    brand_name    = models.CharField("Brand Name",max_length=50)

    class Meta:
        verbose_name='brand'
        verbose_name_plural='brands'


    def __str__(self):
        return '%s' % (self.brand_name)

    def adddate(self, *args, **kwargs):
        if not self.id:
            self.date_created = timezone.localtime(timezone.now())
        self.date_updated = timezone.localtime(timezone.now())

    def save(self, *args, **kwargs):

        self.adddate()
        client = MongoClient()

        db = client.product.brand
        brand_record = {}

        #find table existing entry that code
        db.create_index('brand_name',unique=True)
        brand_record = {'brand_name': self.brand_name ,'created_date':self.date_created}

        try:
            db.insert_one(brand_record)
        except:
            return False






@python_2_unicode_compatible
class Pack_Size(models.Model):
    pack_size_code    = models.IntegerField("Pack Size Code")
    pack_size_name    = models.CharField("Pack Size ",max_length=50)

    class Meta:
        verbose_name='pack_size'
        verbose_name_plural='pack_sizes'


    def __str__(self):
        return '%s' % (self.pack_size_name)

    def adddate(self, *args, **kwargs):
        if not self.id:
            self.date_created = timezone.localtime(timezone.now())
        self.date_updated = timezone.localtime(timezone.now())

    def save(self, *args, **kwargs):

        self.adddate()
        client = MongoClient()

        db = client.product.pack_size
        pack_size_record = {}

        #find table existing entry that code
        db.create_index('pack_size_name',unique=True)
        pack_size_record = {'pack_size_name': self.pack_size_name ,'created_date':self.date_created}

        try:
            db.insert_one(pack_size_record)
        except:
            return False






@python_2_unicode_compatible
class Promotion(models.Model):
    promotion_code    = models.IntegerField("Promotion Code")
    promotion_name    = models.CharField("Promotion Size ",max_length=50)

    class Meta:
        verbose_name='promotion'
        verbose_name_plural='promotions'

    def __str__(self):
        return '%s' % (self.promotion_code)

    def adddate(self, *args, **kwargs):
        if not self.id:
            self.date_created = timezone.localtime(timezone.now())
        self.date_updated = timezone.localtime(timezone.now())

    def save(self, *args, **kwargs):

        self.adddate()
        client = MongoClient()

        db = client.product.promotion
        promotion_record = {}

        #find table existing entry that code
        db.create_index('promotion_name',unique=True)
        promotion_record = {'promotion_name': self.promotion_name ,'created_date':self.date_created}

        try:
            db.insert_one(promotion_record)
        except:
            return False







@python_2_unicode_compatible
class Product_Code(models.Model):



    class Meta:
        verbose_name='product_code'
        verbose_name_plural='product_codes'

    def __str__(self):
        return '%s' % ("product_code")

    def adddate(self, *args, **kwargs):
        if not self.id:
            self.date_created = timezone.localtime(timezone.now())
        self.date_updated = timezone.localtime(timezone.now())

    def save(self, *args, **kwargs):

        self.product_name =args[0]

        self.adddate()
        client = MongoClient()

        db = client.product.product_code

        curs = db.find().sort( [("code", -1)] ).limit(1)

        try:
            code = int(curs[0]['code'])+1
        except:
            code = 0


        product_record = {}

        #find table existing entry that code
        db.create_index('product_name',unique=True)
        db.create_index('code',unique=True)

        product_record = {'code':code,'product_name': self.product_name ,'created_date':self.date_created}

        try:
            db.insert_one(product_record)
        except:
            return False




