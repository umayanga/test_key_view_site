from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, redirect, render
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.utils.timezone import now

from django.shortcuts import render_to_response

from .forms import KeyGenarateForm ,BrandGenarateForm , Pack_SizeGenarateForm , PromotionGenarateForm,Product_CodeGenarateForm
from .models import Key_Gen ,Brand
from .algo import  algo

from pymongo import MongoClient
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from datetime import datetime


#key gen app

def welcome_page(request):
    return render(request, 'marcador/key_welcome_form.html')

@login_required
def key_create(request):

    client = MongoClient()
    db = client.product.product_code
    cursor = db.find()
    #print(cursor)

    if request.method == 'POST':
        form = KeyGenarateForm(data=request.POST)
        expier_date = request.POST['expier_date']
        product_begin_code = request.POST['key_beging_code']

        #print(product_begin_code)
        if(expier_date==""):
            form.is_valid= False;

        if form.is_valid():
             #request.POST._mutable = True
             Key_Gen = form.save(commit=False)
             Key_Gen.save(expier_date,product_begin_code)
             #return redirect('marcador_bookmark_user',username=request.user.username)
             return redirect('marcador_key_home')
        else:
            return False

    else:
        form = KeyGenarateForm()

    context = {'form': form, 'create_key': True ,'product_code':cursor}
    return render(request, 'marcador/key_genarate_form.html', context)


@login_required
def key_view(request):
    client = MongoClient()
    db = client.product.serial_key
    cursor = db.find()
    cursor={'key': cursor}
    return render(request,'marcador/key_list_form.html',cursor)


@login_required
def key_search_view(request):
    client = MongoClient()
    db = client.product.product_code
    cursor = db.find()
    #print(cursor)

    db = client.product.batch_code
    cursor1 = db.distinct("batch_code")
    #print(cursor1)

    context = {'product_code':cursor ,'batch_code':cursor1}
    return render(request, 'marcador/key_search_form.html', context)




@login_required
def key_search_get(request):

    try:
        b_p_code_value = int(request.POST['productCode'])
    except:
        b_p_code_value = ""
        #print('error value _pass int')

    batch_code  = str(request.POST['batchCode'])
    expire_Date  = str(request.POST['expier_date'])
    date_range = str(request.POST['datefilter'])

    if(date_range != ""):
        val =date_range.replace("/","")
        value = val.split(' - ')
        start_Day =datetime.strptime(value[0], "%m%d%Y");
        end_Day   =datetime.strptime(value[1], "%m%d%Y");


    if(expire_Date != ""):
        y =expire_Date.replace("/","")
        expier_Date= datetime.strptime(y, "%m%d%Y");


    client = MongoClient()
    db = client.product.serial_key

    #cursor = db.find({"expier_date":expire_Date,"batch_detail":batch_code})

    if(date_range ==""):
        if(expire_Date == ""):
            if(b_p_code_value !="" and batch_code ==""):
                cursor = db.find({"b_p_code":b_p_code_value})
            elif(b_p_code_value =="" and batch_code !=""):
                cursor = db.find({"batch_detail":batch_code})
            else:
                cursor=db.find({"b_p_code":b_p_code_value, "batch_detail":batch_code})
        else:
            if(b_p_code_value !="" and batch_code ==""):
                cursor = db.find({"expier_date":expier_Date,"b_p_code":b_p_code_value})
            elif(b_p_code_value =="" and batch_code !=""):
                cursor = db.find({"expier_date":expire_Date,"batch_detail":batch_code})
            elif(b_p_code_value =="" and batch_code ==""):
                cursor = db.find({"expier_date":expier_Date})
            else:
                cursor=db.find({"b_p_code":b_p_code_value, "batch_detail":batch_code ,"expier_date" :expire_Date})
    else:
        if(expire_Date == ""):
            if(b_p_code_value !="" and batch_code ==""):
                cursor = db.find({"b_p_code":b_p_code_value ,"created_date":{'$gte': start_Day, '$lt': end_Day}})
            elif(b_p_code_value =="" and batch_code !=""):
                cursor = db.find({"batch_detail":batch_code ,"created_date":{'$gte': start_Day, '$lt': end_Day}})
            elif(b_p_code_value =="" and batch_code ==""):
                cursor = db.find({"created_date":{'$gte': start_Day, '$lt': end_Day}})
            else:
                cursor=db.find({"b_p_code":b_p_code_value, "batch_detail":batch_code ,"created_date":{'$gte': start_Day, '$lt': end_Day}})
        else:
            if(b_p_code_value !="" and batch_code ==""):
                cursor = db.find({"b_p_code":b_p_code_value ,"expier_date":expire_Date ,"created_date":{'$gte': start_Day, '$lt': end_Day}})
            elif(b_p_code_value =="" and batch_code !=""):
                cursor = db.find({"batch_detail":batch_code ,"expier_date":expire_Date,"created_date":{'$gte': start_Day, '$lt': end_Day}})
            elif(b_p_code_value =="" and batch_code ==""):
                cursor = db.find({"expier_date":expier_Date,"created_date":{'$gte': start_Day, '$lt': end_Day}})
            else:
                cursor=db.find({"b_p_code":b_p_code_value, "batch_detail":batch_code ,"expier_date" :expire_Date,"created_date":{'$gte': start_Day, '$lt': end_Day}})

    #print(cursor[1])
    cursor={'key': cursor}
    return render(request,'marcador/key_detail_view_form.html',cursor)


@login_required
def brand_create(request):
    client = MongoClient()
    db = client.product.brand
    cursor = db.find()



    if request.method == 'POST':
        form = BrandGenarateForm(data=request.POST)
        if form.is_valid():
             Brand = form.save(commit=False)
             value = Brand.save()
             if(value==False):
                 return False
             else:
                 return redirect('marcador_brand_create')
        else:
            return False

    else:
        form = BrandGenarateForm()


    context = {'form': form, 'create_brand': True ,'brand': cursor}
    return render(request, 'marcador/brand_entry_form.html', context)




@login_required
def pack_size_create(request):
    client = MongoClient()
    db = client.product.pack_size
    cursor = db.find()



    if request.method == 'POST':
        form = Pack_SizeGenarateForm(data=request.POST)
        if form.is_valid():
             Pack_Size = form.save(commit=False)
             value = Pack_Size.save()
             #print(value)
             if(value==False):
                 return False
             else:
                 return redirect('marcador_pack_size_create')
        else:
            return False

    else:
        form = Pack_SizeGenarateForm()

    context = {'form': form, 'create_pack_size': True ,'pack_size': cursor}
    return render(request, 'marcador/pack_size_entry_form.html', context)



@login_required
def promotion_create(request):
    client = MongoClient()
    db = client.product.promotion
    cursor = db.find()

    if request.method == 'POST':
        form = PromotionGenarateForm(data=request.POST)
        if form.is_valid():
             Promotion = form.save(commit=False)
             value = Promotion.save()
             #print(value)
             if(value==False):
                 return False
             else:
                 return redirect('marcador_promotion_create')
        else:
            return False

    else:
        form = PromotionGenarateForm()

    context = {'form': form, 'create_promotion': True ,'promotion': cursor}
    return render(request, 'marcador/promotion_entry_form.html', context)



@login_required
def product_code_create(request):
    client = MongoClient()

    db = client.product.brand
    cursor = db.find()

    db = client.product.pack_size
    cursor1 = db.find()

    db = client.product.promotion
    cursor2 = db.find()


    db = client.product.product_code
    cursor3 = db.find()


    if request.method == 'POST':


        brand_name =request.POST['brand_name'];
        pack_size  =request.POST['pack_size_name'];
        promotion  =request.POST['promotion_name'];

        product_name = brand_name +" "+ pack_size +"-" +promotion

        form = Product_CodeGenarateForm(data=request.POST)
        if form.is_valid():
             Product_Code = form.save(commit=False)
             value = Product_Code.save(product_name)
             if(value==False):
                 return False
             else:
                 return redirect('marcador_product_code_create')
        else:
            return False

    else:
        form = Product_CodeGenarateForm()


    context = {'form': form, 'create_promotion': True, 'brand' :cursor ,'pack_size':cursor1 ,'promotion':cursor2,'product_code':cursor3}
    return render(request, 'marcador/product_key_code_form.html', context)




@login_required
def batch_detail_view(request):
    client = MongoClient()
    db = client.product.batch_code
    cursor = db.find()
    cursor={'batch': cursor}
    return render(request,'marcador/view_batch_code_form.html',cursor)




#test function
def test(request):
    return render(request,'marcador/test.html')



def loadData(request):
    return render(request,'marcador/test.html')


#####################