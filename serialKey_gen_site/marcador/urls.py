from django.conf.urls import url



urlpatterns = [

    #working urls
    url(r'^$', 'marcador.views.welcome_page', name='marcador_welcome_page'),

    url(r'^create_key/$', 'marcador.views.key_create',
        name='marcador_key_create'),

    url(r'^view_key/$', 'marcador.views.key_view',
        name='marcador_key_view'),

    url(r'^search_key/$', 'marcador.views.key_search_view',
        name='marcador_key_search'),

    url(r'^search_key/key_search', 'marcador.views.key_search_get',
        name='marcador_get_key'),

    url(r'^home/$', 'marcador.views.welcome_page',name='marcador_key_home'),

    url(r'^create_brand/$', 'marcador.views.brand_create',
        name='marcador_brand_create'),

    url(r'^create_pack_size/$', 'marcador.views.pack_size_create',
        name='marcador_pack_size_create'),

    url(r'^create_promotion/$', 'marcador.views.promotion_create',
        name='marcador_promotion_create'),

    url(r'^create_product_code/$', 'marcador.views.product_code_create',
        name='marcador_product_code_create'),

    url(r'^view_batch_detail/$', 'marcador.views.batch_detail_view',
        name='marcador_batch_detail_view'),

    #test urls
    #url(r'^test/$', 'marcador.views.test',name='marcador_test'),



]