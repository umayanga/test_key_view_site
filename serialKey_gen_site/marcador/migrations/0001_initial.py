# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Bookmark',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.URLField()),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(blank=True, verbose_name='description')),
                ('is_public', models.BooleanField(default=True, verbose_name='public')),
                ('date_created', models.DateTimeField(verbose_name='date created')),
                ('date_updated', models.DateTimeField(verbose_name='date updated')),
                ('owner', models.ForeignKey(related_name='bookmarks', to=settings.AUTH_USER_MODEL, verbose_name='owner')),
            ],
            options={
                'ordering': ['-date_created'],
                'verbose_name': 'bookmark',
                'verbose_name_plural': 'bookmarks',
            },
        ),
        migrations.CreateModel(
            name='Key',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key_beging_code', models.CharField(choices=[(0, 'Pedia Pro 250grm Pack'), (1, 'Pedia Pro 400grm Pack'), (2, 'Analene 400grm Pack'), (3, 'Analene 750grm Pack'), (4, 'Anchor 1 kg pack'), (5, 'Analene 750grm Pcak'), (6, 'Magarin '), (7, 'Magarin 500grm Pack'), (8, 'Anmun'), (9, 'Pedia 100grm Pack')], max_length=3)),
                ('key_quantity', models.DecimalField(max_digits=19, verbose_name='key Quantity', decimal_places=10)),
                ('key_code', models.CharField(max_length=50, verbose_name='key Code', unique=True)),
                ('is_public', models.BooleanField(default=True, verbose_name='public')),
                ('date_created', models.DateTimeField(verbose_name='date created', auto_now_add=True)),
                ('date_updated', models.DateTimeField(verbose_name='date updated', auto_now_add=True)),
            ],
            options={
                'ordering': ['-date_created'],
                'verbose_name': 'key',
                'verbose_name_plural': 'keys',
            },
        ),
        migrations.CreateModel(
            name='Key_Gen',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key_beging_code', models.CharField(choices=[(0, 'Pedia Pro 250grm Pack'), (1, 'Pedia Pro 400grm Pack'), (2, 'Analene 400grm Pack'), (3, 'Analene 750grm Pack'), (4, 'Anchor 1 kg pack'), (5, 'Analene 750grm Pcak'), (6, 'Magarin '), (7, 'Magarin 500grm Pack'), (8, 'Anmun'), (9, 'Pedia 100grm Pack')], max_length=3, verbose_name='Product ')),
                ('key_quantity', models.DecimalField(max_digits=19, verbose_name='key Quantity', decimal_places=10)),
                ('key_code', models.CharField(max_length=50, verbose_name='key Code', unique=True)),
                ('is_public', models.BooleanField(default=True, verbose_name='public')),
                ('date_created', models.DateTimeField(verbose_name='date created', auto_now_add=True)),
                ('date_updated', models.DateTimeField(verbose_name='date updated', auto_now_add=True)),
            ],
            options={
                'ordering': ['-date_created'],
                'verbose_name': 'key_Gen',
                'verbose_name_plural': 'keys_Gen',
            },
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True)),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'tag',
                'verbose_name_plural': 'tags',
            },
        ),
        migrations.AddField(
            model_name='bookmark',
            name='tags',
            field=models.ManyToManyField(blank=True, to='marcador.Tag'),
        ),
    ]
